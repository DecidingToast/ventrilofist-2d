
# Ventrilofist-2D

This is a simple unity game using the leapmotion controller to handle a digital marionette.
Art provided by Savion Mercedes. Project management by Daisey Chu.

Demo Video:

[![Ventrilofist Demo](http://img.youtube.com/vi/X3yNyheUk28/0.jpg)](http://www.youtube.com/watch?v=X3yNyheUk28 "Ventrilofist Demo")
