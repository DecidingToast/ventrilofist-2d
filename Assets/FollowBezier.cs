﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FollowBezier : MonoBehaviour {
	
	private enum Direction
	{
		FORWARD, BACKWARD
	};

	[SerializeField]
	private BezierPath path;
	private Rigidbody2D body;
	private float T;
	private float curr = 0f;
	[SerializeField]
	private Direction direction;
	[SerializeField]
	private float delay = 10f;
	// Use this for initialization
	void Start () {
		T = path.points.Count;
		body = GetComponent<Rigidbody2D> ();
	
		switch (direction) {
		case Direction.FORWARD:
			StartCoroutine (forwardUpdate ());
			break;
		case Direction.BACKWARD:
			StartCoroutine (backwardUpdate ());
			break;
		}
	}

	private IEnumerator forwardUpdate ()
	{
		for(;;) {
			if (curr >= T) {
				//yield return new WaitWhile (() => true);
				yield return new WaitForSeconds(delay);
				curr = 0;
				body.MovePosition (path.GetPositionByT (curr));
				print ("Hello");
			} else {
				curr += Time.deltaTime;
				body.MovePosition (path.GetPositionByT (curr));
				yield return null;
			}
		}
	}

	private IEnumerator backwardUpdate ()
	{
		for (;;) {
			if (curr <= 0) {
				yield return new WaitForSeconds (delay);
				curr = T;
				body.MovePosition (path.GetPositionByT (curr));
			} else {
				curr -= Time.deltaTime;
				body.MovePosition (path.GetPositionByT (curr));
				yield return null;
			}
		}
	}
}
