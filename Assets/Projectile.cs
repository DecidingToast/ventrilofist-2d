﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D))]
public class Projectile : MonoBehaviour
{

    private Vector2 startPos;
    public Vector2 direction;
    public float force = 5f;
    public int framesRespawn = 300;
    public int framesDelay = 0;
    private int counter;
	private bool canHit = true;
	// Use this for initialization
	void Start () {
        startPos = transform.position;
        counter = framesDelay;
	}
	
	// Update is called once per frame
	void Update () {
		if(counter == 0)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            GetComponent<Rigidbody2D>().AddForce(direction * force, ForceMode2D.Impulse);
            counter++;
        }
        else if (counter == framesRespawn)
        {
            counter = 0;
            transform.position = startPos;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			canHit = true;
        }
        else
        {
            counter++;
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
		switch(col.collider.tag)
        {
        case "handsfeet":
			GetComponent<SpriteRenderer>().color = Color.green;
			break;
		case "limbs":
			if (canHit) col.gameObject.GetComponent<BreakLimb>().TakeDamage(1);
			break;
		default:
			break;
        }
		canHit = false;
    }
}
