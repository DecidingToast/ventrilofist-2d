﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSpaceMotionParent : MonoBehaviour
{
	private enum handedness {
		LEFT, RIGHT
	};

    [SerializeField]
    Rigidbody2D motionChild;
    [SerializeField]
    Camera myCamera;
    [SerializeField]
    Camera childCamera;
	[SerializeField]
	private float magnetDist = 2f;
	private bool following = false;

	[SerializeField]
	private handedness hand;

	void OnDisable()
	{
		following = false;
		if (motionChild) motionChild.bodyType = RigidbodyType2D.Static;
	}

	void FixedUpdate()
	{
		Vector3 screenPoint = childCamera.ScreenToWorldPoint(myCamera.WorldToScreenPoint(GetComponent<Rigidbody>().position));
		Vector3 childPos = motionChild.transform.position;

		if (Vector2.Distance (screenPoint, childPos) < magnetDist) {
			motionChild.bodyType = RigidbodyType2D.Dynamic;
			following = true;
		}
		if (following) {
			FixedUpdateIfClose ();
			motionChild.bodyType = RigidbodyType2D.Dynamic;
		}
		else {
			motionChild.bodyType = RigidbodyType2D.Static;
		}
	}

	void FixedUpdateIfClose ()
    {
		Vector3 screenPoint = myCamera.WorldToScreenPoint(transform.position);
		if (motionChild)
        {
            if (screenPoint.x > 0 && screenPoint.y > 0 && screenPoint.x < Screen.width && screenPoint.y < Screen.height)
            {
				motionChild.bodyType = RigidbodyType2D.Dynamic;
                motionChild.MovePosition(childCamera.ScreenToWorldPoint(screenPoint));
            }
            else
            {
                motionChild.isKinematic = true;
            }
        }
	}
}
