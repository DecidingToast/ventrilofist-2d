﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakLimb : MonoBehaviour {

	private Joint2D[] joints;
	private Rigidbody2D rigidbody;
	private SpriteRenderer sprite;
	private int currentDurability;

	[SerializeField]
	private int _durability = 3;
	public int Durability
	{
		get { return _durability; }
	}

	public void Awake()
	{
		joints = GetComponents<Joint2D>();
		rigidbody = GetComponent<Rigidbody2D>();
		sprite = GetComponent<SpriteRenderer>();
		currentDurability = Durability;
	}

	public void TakeDamage(int damage)
	{
		Debug.Log ("Taking damage");
		currentDurability -= damage;
		sprite.color = new Color(1, (float)currentDurability/(float)Durability, (float)currentDurability/(float)Durability);
		if (currentDurability <= 0)
		{
			foreach (var joint in joints)
			{
				if (joint != null && joint.attachedRigidbody.tag != "rope") joint.breakForce = 5f;
			}
			rigidbody.AddForce(Vector2.up * 10f, ForceMode2D.Impulse);
		}
	}
}
